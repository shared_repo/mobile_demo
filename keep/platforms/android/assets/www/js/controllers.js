angular.module('starter.controllers', [])

    .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

      // With the new view caching in Ionic, Controllers are only called
      // when they are recreated or on app start, instead of every page change.
      // To listen for when this page is active (for example, to refresh data),
      // listen for the $ionicView.enter event:
      //$scope.$on('$ionicView.enter', function(e) {
      //});

      // Form data for the login modal
      $scope.loginData = {};

      // Create the login modal that we will use later
      $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });

      // Triggered in the login modal to close it
      $scope.closeLogin = function() {
        $scope.modal.hide();
      };

      // Open the login modal
      $scope.login = function() {
        $scope.modal.show();
      };

      // Perform the login action when the user submits the login form
      $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
          $scope.closeLogin();
        }, 1000);
      };
    })

    .controller('PlaylistsCtrl', function($scope) {
      $scope.playlists = [
        { title: 'Reggae', id: 1 },
        { title: 'Chill', id: 2 },
        { title: 'Dubstep', id: 3 },
        { title: 'Indie', id: 4 },
        { title: 'Rap', id: 5 },
        { title: 'Cowbell', id: 6 }
      ];
    })


  //this is my inappbrowser controller:
    .controller('searchCtrl', function($scope, $ionicLoading,$timeout) {
      //$scope.show = function() {
      //  $scope.loading = $ionicLoading.show({
      //    content : 'Loading....'
      //  });
      //};
      //$scope.hide = function() {
      //  $ionicLoading.hide();
      //};

      $scope.webContent = "";
      $scope.webURl="";

      ///////// ****** added *** /////
      $scope.serverResponce = {answer:false};
      $scope.userclickCaptureBtn = {state:false};


      $scope.OpenWebview = function() {
        var ref = window.open('http://www.10dakot.co.il/%D7%A7%D7%A6%D7%99%D7%A6%D7%95%D7%AA-%D7%AA%D7%99%D7%A8%D7%A1-%D7%95%D7%AA%D7%A8%D7%93/', '_blank', 'location=yes');

        ref.addEventListener('loadstart', function(event) {
        });

        ref.addEventListener('loadstop', function(event) {
          //console.log('Web Content on finished : ' + event.content);

        });


      }

    });
